with ada.Text_IO;
use ada.Text_IO;
package body dpila is
   
   procedure pvacia(p: out pila) is
      q1: cola renames p.q1;
      q2: cola renames p.q2;
   begin
      -- Simplemente vaciamos las colas.
      cvacia(q1);
      cvacia(q2);
   end pvacia;
   
   procedure empilar(p: in out pila; x: in elem) is 
      q1: cola renames p.q1;
   begin
      -- Simplemente ponemos un elemento en la cola principal (q1)
      poner(q1,x);
   exception
      when Storage_Error => raise memoria_desbordada;
   end empilar;
   
   procedure desempilar(p: in out pila) is
      q1: cola renames p.q1;
      q2: cola renames p.q2;
      x: elem;
   begin 
      -- Pasamos todos los elementos menos el ultimo desde la 
      -- cola principal (q1) a la cola auxiliar (q2)
      while not esta_vacia(q1) loop
         x:=coger_primero(q1);
         borrar_primero(q1);
         if not esta_vacia(q1) then
            poner(q2,x);
         end if;
      end loop;
      
      -- Pasamos todos los elementos de la cola auxiliar (q2) a 
      -- la cola principal (q1)
      while not esta_vacia(q2) loop
         x:=coger_primero(q2);
         poner(q1,x);
         borrar_primero(q2);
      end loop;
      
   exception
      when Constraint_Error => raise mal_uso;
   end desempilar;
   
   function cima(p: in out pila) return elem is
      q1: cola renames p.q1;
      q2: cola renames p.q2;
      x: elem;
   begin
      -- Aqui pasamos todos los elementos de la cola principal q1
      -- a la cola auxiliar. 
      while not esta_vacia(q1) loop
         x:=coger_primero(q1);
         borrar_primero(q1);
         poner(q2,x);
      end loop;
      -- Notese que tenemos el elemento 'cima' de la pila en x
      
      -- Ahora devolvemos todos los elementos de la cola auxiliar (q2)
      -- a la cola principal (q1)
      while not esta_vacia(q2) loop
         poner(q1,coger_primero(q2));
         borrar_primero(q2);
      end loop;
      -- Devolvemos el elemento x.
      return x;
   exception
      when Constraint_Error => raise mal_uso;
   end cima;
   
   function esta_vacia(p: in pila) return Boolean is
      q1: cola renames p.q1;
   begin
      -- Si la pila principal (q1) esta vacia, entonces la 
      -- pila esta vacia.
      return esta_vacia(q1);
   end esta_vacia;
   
end dpila;
