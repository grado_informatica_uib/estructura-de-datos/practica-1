with dpila;
with ada.Text_IO;
use ada.Text_IO;
procedure Main is
   package mipila is new dpila(Character);
   use mipila;
   p: pila;
   palabra : String := "palabra";
   palabra_inv: String := palabra;
begin
   Put_Line("------------------------------------------------------------------");
   Put_Line("                           VERSION DOS                            ");
   Put_Line("------------------------------------------------------------------");
   Put_Line("");
   Put_Line("Empilar: O(1)");
   Put_Line("Desempilar: O(n)");
   Put_Line("");
   Put_Line("La palabra es: " & palabra);

   -- Recorremos la palabra para empilarla.
   for i in 1..palabra'Length loop
      empilar(p,palabra(i));
   end loop;

   for i in 1..palabra'Length loop
      if not esta_vacia(p) then
         palabra_inv(i):=cima(p);
         desempilar(p);
      end if;
   end loop;

   Put_Line("La palabra invertida es: "& palabra_inv);
   Put_Line("");

end Main;
