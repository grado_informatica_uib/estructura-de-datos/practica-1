with dcola;
generic
   type elem is private;
   
package dpila is
   type pila is limited private;
   mal_uso: Exception;
   memoria_desbordada: Exception;
   
   procedure pvacia(p: out pila);
   procedure empilar(p: in out pila; x: in elem);
   procedure desempilar(p: in out pila);
   function cima(p: in out pila) return elem;
   function esta_vacia(p: in pila) return boolean;
   
private
   
   type nodo;
   type pnodo is access nodo;
   type nodo is record
      x: elem;
      sig: pnodo;
   end record;
   
   package icola is new dcola(elem);
   use icola;
   
   type pila is record
      q1: cola;
      q2: cola;
   end record;
end dpila;
