with ada.Text_IO;
use ada.Text_IO;
package body dpila is
   
   procedure pvacia(p: out pila) is
      q1: cola renames p.q1;
      q2: cola renames p.q2;
   begin
      -- Simplemente vaciamos las colas.
      cvacia(q1);
      cvacia(q2);
   end pvacia;
   
   procedure empilar(p: in out pila; x: in elem) is 
      q1: cola renames p.q1;
      q2: cola renames p.q2;
      y: elem;
   begin
      -- Pasamos todos los elementos desde la cola
      -- q1(cola principal) a la cola q2 (cola auxiliar).
      while not esta_vacia(q1) loop
         y:= coger_primero(q1);
         poner(q2,y);
         borrar_primero(q1);
      end loop;
      
      -- Ponemos el elemento a introducit en la cola q1 (principal)
      poner(q1,x);
      
      -- Movemos todos los elementos de la cola q2 (auxiliar)
      -- a la cola q1 (principal)
      while not esta_vacia(q2) loop
         y:= coger_primero(q2);
         poner(q1,y);
         borrar_primero(q2);
      end loop;
      
   exception
      when Storage_Error => raise memoria_desbordada;
   end empilar;
   
   procedure desempilar(p: in out pila) is
      q1: cola renames p.q1;
   begin 
      -- Simplemente borramos el elemento mmas al borde 
      -- de la cola q1, como ya hemos metido todo para que sea
      -- el elemento de la 'cima' de la pila, no hace falta mas.
      borrar_primero(q1);
   exception
      when Constraint_Error => raise mal_uso;
   end desempilar;
   
   function cima(p: in out pila) return elem is
      q1: cola renames p.q1;
   begin
      -- La cima se corresponde con el coger primero porque al 
      -- empilar ya ordenamos los elementos.
      return coger_primero(q1);
   exception
      when Constraint_Error => raise mal_uso;
   end cima;
   
   function esta_vacia(p: in pila) return Boolean is
      q1: cola renames p.q1;
   begin
      return esta_vacia(q1);
   end esta_vacia;
   
end dpila;
